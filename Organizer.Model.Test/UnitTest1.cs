using Organizer.Model.Serialization;

namespace Organizer.Model.Test
{
     public class Tests
     {
          [SetUp]
          public void Setup()
          {
          }

          [Test]
          public void TestMethodSerialize()
          {
               var model = new DataModel();
               model.Events = new List<Event>() { new Event() { Name = "���� ���������� ������", Date = new DateTime(1980, 10, 12) } };
               model.Projects = new List<Project>() { new Project() { Name = "ϳ�������� BIC" } };
               model.Tasks = new List<Task>() { new Task() { Name = "ϳ�������� �����������" }, new Task() { Name = "Hello world" } };

               DataSerializer.SerializeData(@"D:\Work\University\C3S1\KPZ\KPZ_Lab2\organizer.dat", model);
          }

          [Test]
          public void TestMethodDeserialize()
          {
               var model = DataSerializer.DeserializeData(@"D:\Work\University\C3S1\KPZ\KPZ_Lab2\organizer.dat");
          }
     }
}