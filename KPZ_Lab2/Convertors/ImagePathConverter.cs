﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Organizer.Model;

namespace KPZ_Lab2.Convertors
{
     public class ImagePathConverter:IValueConverter
     {
          Dictionary<string, BitmapImage> cache = new Dictionary<string, BitmapImage>();
          public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
          {
               var type = (DishType)value;
               if (!cache.ContainsKey(type.ToString()))
               {
                    var uri = new Uri(string.Format(@"D:\Work\University\C3S1\KPZ\KPZ_Lab2\KPZ_Lab2\Images\DishType\Type_{0}.png", type), UriKind.Absolute);
                    cache.Add(type.ToString(), new BitmapImage(uri));
               }

               return cache[type.ToString()];
          }

          public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
          {
               throw new NotImplementedException();
          }
     }
}
