﻿using Organizer.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace KPZ_Lab2.Convertors
{
     public class DishTypeToIntConverter : IValueConverter
     {
          Dictionary<string, BitmapImage> cache = new Dictionary<string, BitmapImage>();
          public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
          {
               var type = (DishType)value;

               return (int)type;
          }

          public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
          {
               var type = (int)value;

               return (DishType)type;
          }
     }
}
