﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Organizer.Model;
using KPZ_Lab2.Commands;
using System.Windows.Controls;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace KPZ_Lab2.ViewModels
{
     public class DataViewModel : ViewModelBase
     {
          private DishViewModel _selectedDish;

          private ObservableCollection<DishViewModel> _dishes;

          public ObservableCollection<DishViewModel> Dishes
          {
               get
               {
                    return _dishes;
               }
               set
               {
                    _dishes = value;
                    OnPropertyChanged("Dishes");
               }
          }

          private Command _addDishCommand;
          public Command AddDishCommand
          {
               get
               {
                    return _addDishCommand ??
                         (_addDishCommand = new Command(
                              obj =>
                              {
                                   DishViewModel dish = new DishViewModel();
                                   _dishes.Add(dish);
                                   _selectedDish = dish;
                              }));
               }
          }

          private Command _removeDishCommand;
          public Command RemoveDishCommand
          {
               get
               {
                    return _removeDishCommand ??
                         (_removeDishCommand = new Command(
                              obj =>
                              {
                                   DishViewModel dish = obj as DishViewModel;
                                   if (dish != null)
                                   {
                                        Dishes?.Remove(dish);
                                   }
                              },
                              (obj) => Dishes.Count > 0));
               }

          }

          public DishViewModel SelectedDish
          {
               get
               {
                    return _selectedDish;
               }
               set
               {
                    _selectedDish = value;
                    OnPropertyChanged("SelectedDish");
               }
          }
          public DataViewModel()
          {
          }

     }
}
