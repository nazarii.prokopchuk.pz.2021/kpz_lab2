﻿using Organizer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab2.ViewModels
{
     public class DishViewModel : ViewModelBase
     {
          private string _name;

          public string Name
          {
               get { return _name;}
               set
               {
                    _name = value;
                    OnPropertyChanged("Name");
               }
          }

          private int _price;

          public int Price
          {
               get { return _price; }
               set
               {
                    _price = value;
                    OnPropertyChanged("Price");
               }
          }

          private int _weight;
          public int Weight
          {
               get { return _weight; }
               set
               {
                    _weight = value;
                    OnPropertyChanged("Weight");
               }
          }

          private DishType _type;
          public DishType Type 
          { 
               get { return _type; } 
               set 
               { 
                    _type = value;
                    OnPropertyChanged("Type"); 
               }
          }
     }
}
