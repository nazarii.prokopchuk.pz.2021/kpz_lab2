﻿using Organizer.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using KPZ_Lab2.ViewModels;
using AutoMapper;
using System.Diagnostics;

namespace KPZ_Lab2
{
     /// <summary>
     /// Interaction logic for App.xaml
     /// </summary>
     public partial class App : Application
     {
          private DataModel _model;
          private DataViewModel _viewModel;
          public App()
          {
               /*var mapperConfigMtoV = new MapperConfiguration(cfg => {
                    cfg.CreateMap<DataModel, DataViewModel>();
                    cfg.CreateMap<Dish, DishViewModel>();
                    });
               var mapperMtoV = mapperConfigMtoV.CreateMapper();
               _model = DataModel.Load();
               _viewModel = mapperMtoV.Map<DataModel, DataViewModel>(_model);
               
               var window = new MainWindow() { DataContext = _viewModel};
               window.Show();*/
          }

          public void OnStartup(object sender, StartupEventArgs e)
          {
               var mapperConfigMtoV = new MapperConfiguration(cfg => {
                    cfg.CreateMap<DataModel, DataViewModel>();
                    cfg.CreateMap<Dish, DishViewModel>();
               });
               var mapperMtoV = mapperConfigMtoV.CreateMapper();
               _model = DataModel.Load();
               _viewModel = mapperMtoV.Map<DataModel, DataViewModel>(_model);

               var window = new MainWindow() { DataContext = _viewModel };
               window.Show();
          }
          protected override void OnExit(ExitEventArgs eventArgs)
          {
               try
               {
                    var mapperConfigVtoM = new MapperConfiguration(cfg => {
                         cfg.CreateMap<DataViewModel, DataModel>();
                         cfg.CreateMap<DishViewModel, Dish>();
                         });
                    var mapperVtoM = mapperConfigVtoM.CreateMapper();
                    _model = mapperVtoM.Map<DataViewModel, DataModel>(_viewModel);
                    _model.Save();
               }
               catch(Exception) 
               {
                    base.OnExit(null);
                    throw;
               }

          }
     }
}
