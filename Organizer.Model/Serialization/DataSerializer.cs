﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Organizer.Model.Serialization
{
     public class DataSerializer
     {
          public static void SerializeData(string fileName, DataModel data)
          {
               var formatter = new DataContractSerializer(typeof(DataModel));
               var s = new FileStream(fileName, FileMode.Create);
               formatter.WriteObject(s, data);
               s.Close();
          }

          public static DataModel DeserializeData(string fileName)
          {
               var formatter = new DataContractSerializer(typeof(DataModel));
               var s = new FileStream(fileName, FileMode.Open); 
               return (DataModel)formatter.ReadObject(s);
          }
     }
}
