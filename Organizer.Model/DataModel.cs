﻿using Organizer.Model.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Organizer.Model
{
     [DataContract]
     public class DataModel
     {
          [DataMember]
          public IEnumerable<Dish> Dishes { get; set; }

          public DataModel() 
          {
               Dishes = new List<Dish>() { new Dish() { Name = "Dish name", Price = 0, Weight = 0, Type = DishType.Undefined} };
          }

          public static string DataPath = @"D:\Work\University\C3S1\KPZ\KPZ_Lab2\organizer.dat";

          public static DataModel Load()
          {
               if(File.Exists(DataPath))
               {
                    return DataSerializer.DeserializeData(DataPath);
               }
               return new DataModel();
          }

          public void Save()
          {
               DataSerializer.SerializeData(DataPath, this);
          }
     }
}
