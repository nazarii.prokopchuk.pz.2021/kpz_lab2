﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Organizer.Model
{
     [DataContract]
     public class Dish
     {
          [DataMember]
          public string Name { get; set; }
          [DataMember]
          public int Price { get; set; }
          [DataMember]
          public int Weight { get; set; }
          [DataMember]
          public DishType Type { get; set; }
     }

     [DataContract]
     public enum DishType
     {
          [EnumMember]
          Drink = 0,
          [EnumMember]
          Meat = 1,
          [EnumMember]
          Fish = 2,
          [EnumMember]
          Salad = 3,
          [EnumMember]
          Undefined = 4
     }
}
